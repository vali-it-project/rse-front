import React, {Component} from "react";
import "./App.css";
import Search from "./components/Search.js";
import Results from "./components/Results.js";

class App extends Component {
  state = {
    isSearchConducted: false,
    recipes: []
  };

  handleSearch = recipes => {
    this.setState({isSearchConducted: true, recipes: recipes});
  };

  backToSearch = () => {
    this.setState({isSearchConducted: false});
  };

  searchOrResult = () => {
    if (this.state.isSearchConducted === false) {
      return <Search handleSearch={this.handleSearch} />;
    } else {
      return (
        <Results
          recipes={this.state.recipes}
          backToSearch={this.backToSearch}
        />
      );
    }
  };

  render() {
    return (
      <div className="App">
        <div className="App-background">
          <div className="Search/Result">{this.searchOrResult()}</div>
        </div>
      </div>
    );
  }
}

export default App;
