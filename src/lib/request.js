// FAIL REQUEST.JS ALGUS

// https://github.com/github/fetch/issues/203

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON, status from the response
 */
function parseJSON(response) {
  const {status, statusText, ok} = response;

  return new Promise(resolve =>
    response.json().then((json = {}) =>
      resolve({
        status,
        statusText,
        ok,
        json
      })
    )
  );
}

/**
 * Requests a URL, returning a promise
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *
 * @return {Promise}           The request promise
 */
export default function request(url, options) {
  return new Promise((resolve, reject) => {
    fetch(url, options)
      .then(parseJSON)
      .then(({ok, json, statusText}) => {
        if (ok && json.success !== false) {
          return resolve(json);
        }

        return reject(json.message || statusText);
      })
      .catch(error =>
        reject({
          networkError: error.message
        })
      );
  });
}
