getIngredients = async () => {
  const res = await request("http://localhost:8080/ingredients");
  const ingredients = res.map(({name}) => ({
    label: name,
    value: name
  }));
  // const ingredients = res.map(function(ingredient) {
  //   return {
  //     label: ingredient.name,
  //     value: ingredient.name
  //   };
  // });
  // const trash = [];
  // res.forEach(function(ingredient) {
  //   trash.push({
  //     label: ingredient.name,
  //     value: ingredient.name
  //   });
  // });
  // Array.prototype.map, Array.prototype.reduce, Array.prototype.filter, Array.prototype.forEach

  this.setState({ingredients});
};
