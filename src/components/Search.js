import React, {Component} from "react";
import Select from "react-select";
import request from "../lib/request";

class Search extends Component {
  state = {
    ingredients: [],
    selectedIngredients: []
  };

  componentDidMount() {
    this.getIngredients();
  }

  getIngredients = async () => {
    const res = await request("http://localhost:8080/ingredients");
    const ingredients = res.map(({name}) => ({
      label: name,
      value: name
    }));

    this.setState({ingredients});
  };

  updateSelectedIngredients = ingredientsList => {
    this.setState({selectedIngredients: ingredientsList});
  };

  searchRecipes = async () => {
    // console.log(this.state.selectedIngredients);
    // const keywords = this.state.selectedIngredients
    //   .map(ingredient => ingredient.value)
    //   .join(",");
    let keywords = "";
    for (let i = 0; i < this.state.selectedIngredients.length; i++) {
      keywords += this.state.selectedIngredients[i].value + ",";
    }
    keywords = keywords.replace(/,$/, "");

    const recipes = await request(
      "http://localhost:8080/recipes?keywords=" + keywords
    );
    console.log(recipes);
    this.props.handleSearch(recipes);
  };

  render() {
    return (
      <div className="Background-opacity">
        <div className="App-field">
          <h1 className="Introductory-text">
            Viska pilk külmkappi, mida sealt leiad?
          </h1>
          <div className="Input-fields">
            <Select
              isMulti
              options={this.state.ingredients}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={this.updateSelectedIngredients}
            />
            <br />
            <button
              className="Button2"
              type="submit"
              value="Submit"
              onClick={this.searchRecipes}
            >
              Otsi retsepte!
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Search;
