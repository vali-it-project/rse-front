import React, {Component} from "react";
import Table from "react-bootstrap/Table";

class Results extends Component {
  state = {};

  goBackToSearch = () => {
    this.props.backToSearch();
  };

  render() {
    return (
      <div className="Background-opacity-results">
        <div className="App-field-results">
          <button className="HomeButton" onClick={this.goBackToSearch}>
            Tagasi otsingusse
          </button>
          <h1 className="RecipeHeading">Võiksid valmistada:</h1>

          <div className="Recipes">
            {this.props.recipes.map(recipe => {
              return (
                <Table size="sm">
                  <thead className="Recipe">
                    <th className="FirstColumne align-middle">
                      <p className="RecipeName">{recipe.name}</p>
                      <p className="MatchCount">
                        {" "}
                        Selle retseptiga saad kasutada{" "}
                        {recipe.ingredientMatchCount} olemasolevat koostisosa!
                      </p>
                      <a
                        className="Url"
                        target="_blank"
                        href={recipe.recipeUrl}
                      >
                        <span>Vaata retsepti</span>{" "}
                      </a>
                    </th>
                    <br />
                    <th>
                      <img
                        className="RecipeImage"
                        src={recipe.recipeImage}
                        alt={recipe.name}
                      />
                    </th>
                  </thead>
                </Table>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Results;
